# Odoo 12 + Facturación Electrónica

Estructura de archivos y Script listos para despliegue de Odoo 12 con Docker + módulos de facturación electrónica


### Requisitos

* maquina linux (probado sobre ubuntu 18.04)
* acceso superusuario
* openssh 
* git
* acceso al repositorio https://gitlab.com/maicoldlb/addons_facturalo_12

### Instalación

Acceder como superusuario

```
sudo su
```

clonar el repositorio dentro de un directorio de su preferencia, por ejemplo /home/ubuntu/odoo/

```
git clone https://gitlab.com/rash07/facturaloperu_odoo_12.git
```

si posee agregada una clave ssh a Gitlab ejecute en vez del comando anterior

```
git clone git@gitlab.com:rash07/facturaloperu_odoo_12.git
```

acceda a la carpeta clonada

```
cd facturaloperu_odoo_12
```

conceda permisos de ejecución al archivo install.sh

```
chmod +x install.sh
```

puede ejecutar el script con el siguiente comando, obtendrá Odoo ejecutandose en el puerto 8069, PostgreSql por el puerto 5432 y la clave de super usuario de Odoo será "odoo"

```
./install.sh
```

si desea cambiar los puertos anteriores ejecute entonces el script con la siguiente sintaxis

```
script [clave de super usuario Odoo] [puerto Odoo] [puerto PostgreSQL]
```

un ejemplo sería

```
./install.sh administrador 80 5433
```

en el proceso de ejecución se solicitarán las credenciales para el repositorio que contiene los módulos, deberá ingresar su correo de acceso a Gitlab y contraseña posteriormente.
verifique que su acceso a los repositorios necesarios, una vez finalizado el script tendrá Odoo funcionando correctamente en la IP o Dominio bajo el puerto configurado previamente.

### Actualización

Acceder como superusuario

```
sudo su
```

acceda a la carpeta del proyecto

```
cd facturaloperu_odoo_12
```

ejecute el comando git para actualizar

```
git pull origin master
```

se le solicitarán las credenciales para poder bajar los nuevos cambios en los archivos

posteriormente acceda a la carpeta addons

```
cd addons
```

ejecute el comando git para actualizar

```
git pull origin master
```

de nuevo se le solicitarán las credenciales, al finalizar ejecute el comando para reiniciar el servicio de Docker

```
docker restart fp_odoo
```

luego de finalizar el reinicio, podrá acceder a su web odoo y actualizar la lista de aplicaciones o módulo que posea los nuevos cambios

### Tareas del script

* Actualizar el sistema operativo
* Instalar las herramientas necesarias (Docker, Curl, entre otras)
* Clonar los repositorios al cual se tenga acceso como miembro
* Configurar los archivos de los contenedores para que los módulos extras sean reconocidos por Odoo
* Iniciar los servicios de base de datos y web para Odoo 12

### Tareas comunes posteriores a la ejecución del script

1. Verificar el acceso

Acceder al dominio o IP, Odoo deberá estar listo con una base de datos precargada, de tener algun error (error 500, 404 o similar) debe reiniciar el servicio con el comando

```
docker restart fp_odoo
```

Si continua con algun error puede ubicar el log de Odoo con el siguiente comando

```
docker logs --tail 60 fp_odoo
```

Puede hacernos llegar el log o enviar el acceso a su instancia para verificar si el problema no es solventado reiniciando el servicio

2. Crear su primera BD 

Eliminar la base de datos actual, esta contiene ya datos precargados sin pero esta en otro idioma y con otras localidades, se recomienda eliminarla y crear una nueva con el idioma y país adecuado

3. Módulos extras

Si desea agregar módulos extras deberá añadirlos a la carpeta addons, una vez cargados reinicie el servicio y actualice las aplicaciones en Odoo, recuerde estar en modo desarrollador

4. Comandos Docker

El despliegue se realiza con Docker, por lo que necesitará conocer algunos comandos básicos para su mantenimiento.
Puede ver la documentacion aqui https://docs.docker.com

5. Casos de conflictos con Git

Si en una actualización al ejecutar "git pull origin master" le muestra conflicto porque posee archivos editados, por ejemplo config/odoo.conf, deberá ejecutar el siguiente comando tomando en cuenta el punto al final

```
git checkout .
```

6. Instalación de librerias de python

acceda a la carpeta clonada

```
cd facturaloperu_odoo_12
```

sus módulos extras pueden requerir librerías que no se encuentran instaladas, para ello puede seguir el siguiente ejemplo, tomando en cuenta que "web" hace referencia al nombre del contenedor

```
docker-compose exec -T web pip3 install xlrd
```

en una instalación sin Docker puede utilizar solamente

```
pip3 install xlrd
```
## Manuales

- Manual de instalación - despliegue con Docker

https://docs.google.com/document/d/1hSZeGkUpIkCq-cMzEhKgmpntNhH5h0dXQfGsBNcpvsg/edit?usp=sharing

- Gestion de la Base de datos - configuración inicial

https://docs.google.com/document/d/1X5sOT5cunbDi3Kuebr010S4ciQ_YDM_NdTAuLLb-uBo/edit?usp=sharing

- Flujos de trabajo

https://docs.google.com/document/d/1I0EwLBU4dik8JX3zvlc1hx9S8XdnTYNOlW5Bp_uL1gk/edit?usp=sharing

## Videos (configuración)

https://loom.com/share/folder/50bbd76fec3f4a19a070f2faf8019a9a

Autor
-----

http://facturaloperu.com
