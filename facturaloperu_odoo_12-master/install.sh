#!/bin/bash

ADMIN_PASSWORD=${1:-'odoo'}
ODOO_PORT=${2:-'8069'}
POSTGRESQL_PORT=${3:-'5432'}

echo "Updating system"
apt-get -y update
apt-get -y upgrade

echo "Installing git"
apt-get -y install git-core

echo "Installing docker"
apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get -y update
apt-get -y install docker-ce
systemctl start docker
systemctl enable docker

echo "Installing docker compose"
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "Cloning modules"

rm -rf "$PWD/addons"
git clone https://gitlab.com/maicoldlb/addons_facturalo_12.git addons

echo "Configuring Docker to deploy"

cat << EOF >> $PWD/docker-compose.yml
version: '3'

services:
    web:
        image: odoo:12.0
        container_name: fp_odoo
        ports:
            - "$ODOO_PORT:8069"
        depends_on:
            - db
        volumes:
            - fp-web-data:/var/lib/odoo
            - ./config:/etc/odoo
            - ./addons:/mnt/extra-addons
    db:
        image: postgres:10
        container_name: fp_bd
        ports:
            - "$POSTGRESQL_PORT:5432"
        environment:
            - POSTGRES_DB=postgres
            - POSTGRES_USER=odoo
            - POSTGRES_PASSWORD=odoo
            - PGDATA=/var/lib/postgresql/data/pgdata
        volumes:
            - fp-db-data:/var/lib/postgresql/data/pgdata
volumes:
    fp-web-data:
    fp-db-data:
EOF

sed -i '/admin_passwd/d' $PWD/config/odoo.conf

cat << EOF >> $PWD/config/odoo.conf


admin_passwd = $ADMIN_PASSWORD
EOF


echo "Settings and deployment"
docker-compose up -d
docker-compose exec -T web pip3 install phonenumbers
docker-compose exec -T web pip3 install xlrd